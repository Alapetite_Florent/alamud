# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2,Action3
from mud.events import BuyWithEvent

class BuyWithAction(Action3):
    EVENT = BuyWithEvent
    ACTION = "buy-with" 
    RESOLVE_OBJECT = "resolve_for_take"
    RESOLVE_OBJECT2 = "resolve_for_use"
    