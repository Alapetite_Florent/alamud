# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2,Action3
from mud.events import CaressEvent

class CaressAction(Action2):
    EVENT = CaressEvent
    ACTION = "caress" 
    RESOLVE_OBJECT = "resolve_for_operate"
    
    