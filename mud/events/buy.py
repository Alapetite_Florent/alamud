# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2,Event3

class BuyWithEvent(Event3):
    NAME = "buy-with"

    def perform(self):
        if not self.object.has_prop("buyable"):
            self.object.add_prop("object-not-buyable")
            return self.buy_failed()
        if not self.object2.has_prop("money"):
            self.object2.add_prop("object2-not-money")
            return self.buy_failed()
        if self.object in self.actor:
            self.add_prop("object-already-in-inventory")
            return self.buy_failed()
        self.object.move_to(self.actor)
        self.inform("buy-with")
        
        

    def buy_failed(self):
        self.fail()
        self.inform("buy-with")
