# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2,Event3


class CaressEvent(Event2):
    NAME = "caress"

    def perform(self):
        if not self.object.has_prop("pet"):
            self.fail()
            return self.inform("caress.failed")        
        if not self.object.has_prop("satieted"):            
            self.inform("caress")
            self.inform("death")
            cont = self.actor.container()
            for x in list(self.actor.contents()):
                x.move_to(cont)
            self.actor.move_to(None)
        else:
            self.inform("caress")
            
        