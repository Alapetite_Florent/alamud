# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2,Event3


class EatEvent(Event2):
    NAME = "eat"

    def perform(self):
        if not self.object.has_prop("eatable"):
            self.fail()
            return self.inform("eat.failed")
        self.inform("eat")
        self.inform("death")
        cont = self.actor.container()
        for x in list(self.actor.contents()):
            x.move_to(cont)
        self.actor.move_to(None)