# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2,Event3

class GiveToEvent(Event3):
    NAME = "give-to"

    def perform(self):
        if not self.object2.is_container():
            self.add_prop("object2-not-container")
            return self.give_failed()
        if self.object not in self.actor:
            self.add_prop("object-not-in-inventory")
            return self.give_failed()        
        if not self.object.has_prop("eatable"):
            self.add_prop("object-not-givable")
            return self.give_failed()
        if not self.object2.has_prop("hungry"):
            self.add_prop("object-not-givable")
            return self.give_failed()
        self.object.move_to(self.object2)
        self.inform("give-to")
        
        

    def give_failed(self):
        self.fail()
        self.inform("give-to.failed")
